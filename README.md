# Blueprint for 5G NSA and 5G SA Campus Network Testbeds

In this repository you will find full lists of the hardware used in our 5G NSA and 5G SA campus network testbed.

You can find for vendor Nokia a configuration [here](configurations/Nokia/README.md) and for vendor Ericsson [here](configurations/Ericsson/README.md).

If you are interested in these configuration, please contact [Nokia](https://www.nokia.com/) or [Ericsson](https://www.ericsson.com/) to get more information.