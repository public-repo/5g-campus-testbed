# 5G NSA and 5G SA Campus Testbed: Vendor Nokia

## General Information

---

If you are interested in the hardware, please contact [Nokia](https://www.nokia.com/).

---

The Network consists of a 19" server rack with at least 8 Rack units. The following Hardware is required:

1. Switch (1RU) to connect Basebands, Server and external connections
2. BaseBand Unit Case (3 RU)  
   1. Left Side
      1. 4G System Module
         to connect 4G Baseband and EPC
      2. 4G Baseband Module (ABIA)
         to connect 4G System Module
   2. Right Side
      1. 5G System Module to connect 5G Baseband and EPC/5GC
      2. 5G Baseband Modules - 2 Pieces
          to connect 5G System Module
3. Indoor Radio Control - AirScale Indoor Hub (1 RU)
4. Server for EPC (2 RU) to host EPC and/or 5G SA Core
5. Multi-Access Edge Cloud Server (2 RU)
   for Applications
6. Power Supply (1 RU)
   to supply hardware with 48V DC (next to general power supply, e.g., 230V)

For the RAN, different antennas are required for

1. Outdoor 4G (only needed for 5G NSA)
2. Outdoor 5G
3. Indoor Radio System (4G/5G)

## Core - Hardware Set-up

1.  Switch - WPX 210

2. BaseBand Unit Case - AirScale AMIA

   Rack case for 3 rack units with left and right side shelves. Can hold up to 3 basebands and 1 system module per side.

   <img src="./Figures/amia.png" width="50%"/>

   source: [Nokia AirScale Base Station Product Description](https://kupdf.net/download/nokia-air-scale-sm-product-description-pdf_58a154d56454a7e060b1e924_pdf)

   1. Left Side

      1. 1 LTE System Module - ASIA

         Common plug-in System unit for Airscale subrack, with centralized control processing and integrated Ethernet transport termination per subrack side.

         <img src="./Figures/asia.png" width="50%"/>

         source: [Nokia AirScale Base Station Product Description](https://kupdf.net/download/nokia-air-scale-sm-product-description-pdf_58a154d56454a7e060b1e924_pdf)

      2. LTE Baseband Module - ABIA

         Capacity plug-in Baseband unit with cell specific baseband processing. RF Module connectivity. 6 x OBSAI/CPRI Ports with up to 9.8Gbps.

         <img src="./Figures/abia.png" width="50%"/>

         source: [Nokia AirScale Base Station Product Description](https://kupdf.net/download/nokia-air-scale-sm-product-description-pdf_58a154d56454a7e060b1e924_pdf)

   2. Right Side

      1. 5G System Module - ASIK

         Common plug-in System unit for Airscale subrack, with centralized control processing and integrated Ethernet transport termination per subrack side.

         <img src="./Figures/asik.png" width="50%"/>

         source: [Nokia AirScale System Module Product Description](http://www.cosconor.fr/GSM/Divers/Equipment/Nokia/Airscale%20system%20module%20product%20description.pdf)

      2. 5G Baseband Module - ABIL

         Capacity plug-in Baseband unit with cell specific baseband processing. RF Module connectivity. 6 x OBSAI/CPRI Ports with up to 9.8Gbps.

         <img src="./Figures/abil.png" width="50%"/>

         source: [Nokia AirScale System Module Product Description](http://www.cosconor.fr/GSM/Divers/Equipment/Nokia/Airscale%20system%20module%20product%20description.pdf)

3. Indoor Radio Control - AsiR-HUB

   The 5G ASiR System solution provides an enhanced SFN (Single Frequency Network) indoor solution addressing key requirements such as lower cost of ownership, smaller product form factor, better indoor coverage, and macro-grade level larger capacity. 

   <img src="https://www.nokia.com/sites/default/files/styles/scale_1440_no_crop/public/2019-04/asir-shub.png" width="50%"/>

   Source: [Nokia.com AirScale indoor radio system](https://www.nokia.com/networks/products/airscale-indoor-radio-system/)

   - 12 x 10GBASE-T interface with RJ-45 connectors  
   - CPRI over 10GBASE-T fronthaul  
   - 4 x SFP+ ports  
   - 9.8Gbps CPRI interface  
   - Maintenance Port

4. Server for EPC - Fujitsu RX2540 M5

   <img src="https://www.fujitsu.com/de/Images/W-DK63985_tcm20-4303931.png" width="50%"/>

   source: [Fujitsu.com FUJITSU Server PRIMERGY RX2540 M5](https://www.fujitsu.com/de/products/computing/servers/primergy/rack/rx2540m5/)

   - 2x Intel® Xeon® Gold 6125  
   - 512 GB DIMM, 2666MT/s, Dual Rank  
   - 5x 1 TB 10K RPM SAS 12Gbps 512e 2.5in Hot-plug Hard Drive  
   - 1x PERC H740P RAID Controller, 8GB NV Cache, Minicard  
   - 1x Intel Ethernet I350 QP 1Gb Network Daughter Card  
   - 1x Intel Ethernet I350 QP 1Gb Network Card  

5. Multi-Access Edge Cloud Server - Fujitsu RX2540 M5

   <img src="https://www.fujitsu.com/de/Images/W-DK63990_tcm20-4303934.png" width="50%"/>

   source: [Fujitsu.com FUJITSU Server PRIMERGY RX2540 M5](https://www.fujitsu.com/de/products/computing/servers/primergy/rack/rx2540m5/)

   - 2x Intel® Xeon® Gold 6125  
   - 512 GB DIMM, 2666MT/s, Dual Rank  
   - 5x 1 TB 10K RPM SAS 12Gbps 512e 2.5in Hot-plug Hard Drive  
   - 1x PERC H740P RAID Controller, 8GB NV Cache, Minicard  
   - 1x Intel Ethernet I350 QP 1Gb Network Daughter Card  
   - 1x Intel Ethernet I350 QP 1Gb Network Card  

6. Power Supply - Generic AC 230V - DC -48V

   The Power Supply generates the -48V power for AirScale SubRack.

## RAN - Hardware Set-up

1. Outdoor LTE - FZHN (B38 LTE)

   <img src="https://fccid.io/png.php?id=3199976&page=0" width="50%"/>

   source: [FCCID.io ](https://fccid.io/VBNFZHN-01/External-Photos/FZHN-external-photos-3199976)

   - RF Output Power: 20W per TX Pipe
   - TX/RX: 8T8R
   - Band/Frequency Range: B41:  2496MHz ~ 2690MHz 
   - BW / OWB: 194 MHz / 60 MHz

2. Outdoor 5G - AZQH

   - Max RF Output Power: 320W (40W per TRX)
   - TX/RX: 8T8R
   - Band/Frequency Range: B43:  3600 -3800 MHz
   - IBW / OWB: 100 MHz/100MHz

3. Indoor Radio System - 4G AHGEHA / 5G ASiR-ANT

   - Band: TDD n78 (B42, B43)
   - TX/RX: 4T4R MIMO 
   - Max RF Output Power: Up to 250mW per Tx Branch
   - Carrier BW: 50, 100 MHz

   <img src="https://www.nokia.com/sites/default/files/styles/scale_1440_no_crop/public/2019-04/asir-prrh-1.jpg" width="50%"/>

   Source: [Nokia.com AirScale indoor radio system](https://www.nokia.com/networks/products/airscale-indoor-radio-system/)

## Other Information

1. Cabling Sequence
   1. Internal
   2. External
   3. Power