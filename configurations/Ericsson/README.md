# 5G private network (campus network) - 5G Non-Stand-Alone (NSA) with Ericsson Hardware

## General Information

The network consists of a 19" server rack with at least 10 rack units.

The following (computing) Hardware is required for the testbed:
* Router (1 RU) - to connect Basebands, Server and external connections
* Baseband LTE (1 RU) - to connect LTE Antennas and to EPC and 
* Baseband 5G (1 RU) - to connect 5G Antennas and to EPC
* Indoor Radio Unit (RU) - to connect indoor antennas and connect to the basebands
* Server for EPC (1 RU) - to host Evolved Packet Core
* Server for Applications (1 RU) - to host on Services
* Power Supply (2 RU) - to supply hardware with 48V DC

For the RAN different antennas are required for the use of:
* Outdoor LTE
* Outdoor 5G
* Indoor Radio System


## Hardware Setup of the Core 

### Border Gateway Router 6675

The Router 6675 chassis holds up to 20 traffic ports, 6 other ports including management port, console port, alarm port, sync ports, and USB port.

Specific information about this product can be found [on Ericssons website](https://mediabank.ericsson.net/deployedFiles/ericsson.com/Router%206675%20Datasheet%20Rev%20M.pdf).

<img src="./figures/BGR6675.png" width="50%"/>

Source: [Ericsson Router 6675 Datasheet](https://mediabank.ericsson.net/deployedFiles/ericsson.com/Router%206675%20Datasheet%20Rev%20M.pdf)


### Baseband Unit 5216

Baseband 5216 (5212 or 6620/6630 can be used as well) is an eNodeB components used in Private Network Solution such as our 5G Campus Network.

Specific information about this product can be found [on this website](https://www.tempesttelecom.com/products/ericsson-baseband-5216-kdu137925-31/).

<img src="./figures/BBU5216.png" width="50%"/>

Source: [Tempest Telecom Solutions](https://www.tempesttelecom.com/products/ericsson-baseband-5216-kdu137925-31/)


### Baseband Unit 6630 - Our 5G Baseband

One Baseband 6630 supports NR RAN and is the gNodeB component in the Private Network solution including 5G NSA support.

<img src="./figures/BBU6630.png" width="50%"/>

Source: [Tempest Telecom Solutions](https://www.tempesttelecom.com/products/ericsson-baseband-6630-kdu137848-11/)


### IRU 8846 - The Indoor Radio Unit

The Radio Dot System (RDS) provides indoor broadband coverage for small-cell markets, mainly in medium to large buildings. The IRU and RDs are connected through a Radio Dot Interface (RDI) cable, which is used for both communication and power supply.

The Baseband 6630 is connected to the IRU via CPRI. The IRU connects to the Radio dots (small cells) via an RDI.

<img src="./figures/IRU8846.jpg" width="50%"/>

Source: [Website](https://sc01.alicdn.com/kf/H7935fd2ed8a341c2818fe4283667638dj.jpg)


### Enterprise Core Server Dell R640 - Server vor EPC

The Dell R640 server used for Enterprise Core deployment in Private Networks runs the Ericsson Cloud Execution Environment, which is in charge of authentication and orchestration of the network. This Server has the following configuration: 
- Intel Xeon Gold 6138 2.0G, 20C/40T, 10.4GT/s 2UPI, 27M Cache, Turbo, HT (125W) 
- 12x 32GB RDIMM 2666MT/s Dual Rank 
- 4x 2.4TB 10K RPM SAS 12Gbps 512e 2.5in Hot-plug Hard Drive 
- PERC H730P RAID Controller, 2GB NV Cache, Mini card 
- Intel X710 DP 10Gb DA/SFP+, + I350 DP 1Gb Ethernet, Network Daughter Card 

Specific information about this product can be found [in this document](https://i.dell.com/sites/csdocuments/Shared-Content_data-Sheets_Documents/en/poweredge-r640-spec-sheet.pdf).

<img src="./figures/R640.png" width="50%"/>

Source: [Dell.com](https://i.dell.com/sites/csdocuments/Shared-Content_data-Sheets_Documents/en/poweredge-r640-spec-sheet.pdf)

### Multi-Purpose Server Dell R640

The Multi-Purpose Server based on Dell EMC PowerEdge R640, has the following configuration: 
- 2x Intel® Xeon® Gold 5118, 2.3G, 12C/24T, 10.4GT/s, 16M Cache, Turbo, HT (105W) 
- 6x 32GB RDIMM, 2666MT/s, Dual Rank 
- 4x 2.4TB 10K RPM SAS 12Gbps 512e 2.5in Hot-plug Hard Drive 
- 1x PERC H740P RAID Controller, 8GB NV Cache, Minicard 
- 1x Intel Ethernet I350 QP 1Gb Network Daughter Card 
- 1x Intel Ethernet I350 QP 1Gb Network Card 

Multi-Purpose Server is a mandatory component to co-locate application services on a single platform. 


### PBC 6200 and Power Shelf 150 - Power Supply

The PBC 6200 is homed in a Power Shelf 150. This in combination is the power supply for our mobile testbed.
Th hardware offers us the following features:
- power and battery supply
- variable configuration
- power unit converts incoming AC power to -48V DC
- is controlled and supervised by a Control and Supervision
Unit (CSU
- maximum load of 800 A for Main+Extension configuration

The Power Shelf 150 (PS 150) is supplying 48V DC power and it is designed for installation in any suitable 19” or 23” rack space available at a site having only 2U high, thus requiring very limited space. 

The PS 150 controller is equipped with an integrated display and keypad for easy system access on site. The controller also features binary alarms that can be connected to the site’s alarm collecting system. 

Specific information about the PBC 6200 can be found [in this document](https://studylib.net/doc/25200913/pbc-6200).

<img src="./figures/pbc6200.png" width="50%"/>

Source: [studylib.net](https://studylib.net/doc/25200913/pbc-6200)


## Hardware Setup of the Radio Access Network (RAN)

### Radio Unit 2218 or Radio Unit 8823 (3400-3800 MHz) - Outdoor Radio

Private Networks offers outdoor Radio solution consisting of following components: Baseband, Remote Radio Unit, Antenna. The quantity and model of the component varies from customer to customer according to their requirements. 

Depending on the frequency allocated by the National Authority responsible for frequency administration, the appropriate model of radio unit shall be selected. 
Radio access connectivity is realized between any port of the Baseband 6620/6630 and port “E” of the Remote Radio Unit 2218. From the Baseband location, optical fibers (Single Mode) shall reach the Remote Radio Units. All Radio are connected to Baseband. 

Radio 2218:
- MIMO: 2T2R 
- Maximum nominal output: 2x80W
- Number of Carriers: One to three carriers
- 3GPP FDD bands
More information about the Radio 2218 can be found in the [linked dokument](https://www.manualslib.com/manual/1523444/Ericsson-2217.html).

<img src="./figures/R2218_1.png" width="25%"/><img src="./figures/R2218_2.png" width="25%"/>

Source: [studylib.net](https://studylib.net/doc/25200913/pbc-6200)

Radio 8823:
- MIMO: 8T8R 
- Maximum nominal output: 8x20W
- Frequency Range: 3.5GHz band
- 3GPP FDD bands B42, B43
More information about the Radio 8823 can be found in this [Youtube Video](https://www.youtube.com/watch?v=Z9DzfH9FwP4).

<img src="./figures/R8823.png" width="50%"/>

### Comba Outdoor Directional Single-band Antenna ODV-065R18J-GQ  (LTE) - Outdoor Radio

- Directional Base Station Sector Antenna
- Frequency Range: 1.71 to 2.69 GHz
- Single Band
- Dual Slant (±45 Degrees) Polarization
- 18.5 dBi Gain, E-Tilt: 0-10°
- X Pol, 65 Degree Horizontal Beamwidth
- Pole Mount

More information can be found on [Comba Telecom Website](https://www.comba-telecom.com/en/products-and-services/antennas/base-station-antenna/pena-band-antenna-10-ports/item/2344-odi-065r18jkdkd02-gq_v1) and as a [PDF document here](https://cdn.everythingrf.com/live/ODV-065R18J-G%20V2.pdf).

<img src="./figures/comba.png" width="50%"/>

Source: [Comba Website](https://www.comba-telecom.com/images/Product/ODI-065R18JKDKD02-GQ_V1.png)


### 5G Dot (Dot 4479) - Indoor Radio

- Speed per Dot module: 2 Gbps
- 4T4R
- 100 MHz IBW
- 4x 250 mW (TDD)

More information about this hardware can be found on the [Ericsson Website](https://www.ericsson.com/en/networks/offerings/5g/5g-supreme-indoor-coverage) and [in this presentation](https://mediabank.ericsson.net/deployedFiles/ericsson.com/Taking%20the%20next%20step%20in%20the%20indoor%20revolution.pdf).

<img src="./figures/Dot4479.png" width="50%"/>

Source: [Ericsson Website](https://www.ericsson.com/en/networks/offerings/5g/5g-supreme-indoor-coverage)


## Other Information

### Cabling Sequence
1. Internal
2. External
3. Power

### GPS Synchronisation

The Global Navigation Satellite System (GNSS) receiver system for the basebands receives a GNSS timing signal that is used for synchronization of the Baseband. 
The main components in the GNSS receiver system for Baseband is the GNSS active antenna and the GNSS receiver unit.
The GNSS active antenna receives signals from the GNSS satellites which are sent through the RF cables to the GNSS receiver units. 
The GNSS receiver unit sends One Pulse Per Second (1PPS) and time of day sentences towards the Basebands. By that, the Baseband is synchronized to the timing received from the GNSS receiver unit. 

### Wiring Plan for Trial Setup

<img src="./figures/wiring_plan.png" width="50%"/>